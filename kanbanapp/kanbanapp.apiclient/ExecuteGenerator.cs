using NUnit.Framework;

namespace kanbanapp.apiclient;

[TestFixture]
public class ExecuteGenerator
{
    [Test]
    public async Task Execute() {
        await Generator.Generate();
    }
}