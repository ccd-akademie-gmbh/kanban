﻿using NSwag;
using NSwag.CodeGeneration.CSharp;

namespace kanbanapp.apiclient;

public static class Generator
{    
    public static async Task Generate() {
        var document =  await OpenApiDocument.FromUrlAsync("http://localhost:5059/swagger/v1/swagger.json");
        
        var settings = new CSharpClientGeneratorSettings {
            ClassName = "KanbanClient",
            CSharpGeneratorSettings = {
                Namespace = "kanbanapp.apiclient"
            }
        };

        var generator = new CSharpClientGenerator(document, settings);
        var code = generator.GenerateFile();
        File.WriteAllText("../../../../kanbanapp.apiclient/KanbanClient.cs", code);
    }
}