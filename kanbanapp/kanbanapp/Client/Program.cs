using Blazorise;
using Blazorise.Bootstrap5;
using Blazorise.Icons.FontAwesome;
using kanbanapp.Client.apiclient;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace kanbanapp.Client;

internal static class Program
{
    public static async Task Main(string[] args) {
        var builder = WebAssemblyHostBuilder.CreateDefault(args);
        builder.RootComponents.Add<App>("#app");
        builder.RootComponents.Add<HeadOutlet>("head::after");

        builder.Services
            .AddBlazorise( options => { options.Immediate = true; })
            .AddBootstrap5Providers()
            .AddFontAwesomeIcons();
        
        builder.Services.AddScoped(_ => 
            new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
        builder.Services.AddScoped(serviceProvider => 
            new KanbanClient(serviceProvider.GetService<HttpClient>()!));

        await builder.Build().RunAsync();
    }
}