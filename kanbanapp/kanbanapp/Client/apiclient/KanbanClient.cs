using kanbanapp.contracts;
using Newtonsoft.Json;

namespace kanbanapp.Client.apiclient;

public class KanbanClient
{
    private readonly HttpClient _httpClient;

    public KanbanClient(HttpClient httpClient) {
        _httpClient = httpClient;
    }

    public async Task<Board> GetBoardAsync() {
        var response = await _httpClient.GetAsync("api/items");
        var board = await GetBoard(response);
        return board;
    }

    public async Task<Board> CreateNewAsync(Item item) {
        var itemJson = JsonConvert.SerializeObject(item);
        var response = await _httpClient.PostAsync("api/items", new StringContent(itemJson));
        var board = await GetBoard(response);
        return board;
    }

    public async Task<Board> LeftAsync(Guid id) {
        var response = await _httpClient.PostAsync($"api/items/left/{id}", null);
        var board = await GetBoard(response);
        return board;
    }

    public async Task<Board> RightAsync(Guid id) {
        var response = await _httpClient.PostAsync($"api/items/right/{id}", null);
        var board = await GetBoard(response);
        return board;
    }

    public async Task<Board> UpAsync(Guid id) {
        var response = await _httpClient.PostAsync($"api/items/up/{id}", null);
        var board = await GetBoard(response);
        return board;
    }

    public async Task<Board> DownAsync(Guid id) {
        var response = await _httpClient.PostAsync($"api/items/down/{id}", null);
        var board = await GetBoard(response);
        return board;
    }

    private static async Task<Board?> GetBoard(HttpResponseMessage response) {
        var json = await response.Content.ReadAsStringAsync();
        var board = JsonConvert.DeserializeObject<Board>(json);
        return board;
    }
}