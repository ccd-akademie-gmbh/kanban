using System.Reflection;
using kanbanapp.integration;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using Microsoft.OpenApi.Models;

namespace kanbanapp.Server;

internal static class Program
{
    public static void Main(string[] args) {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddSingleton<Interactors>();
        
        builder.Services
            .AddDataProtection()
            .PersistKeysToFileSystem(new DirectoryInfo(@"data-protection-keys"))
            .UseCryptographicAlgorithms(new AuthenticatedEncryptorConfiguration {
                EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
                ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
            });

        AddSwagger(builder);

        builder.Services
            .AddControllers()
            .AddNewtonsoftJson();

        // Add services to the container.
        //builder.Services.AddControllersWithViews();
        builder.Services.AddRazorPages();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment()) {
            app.UseWebAssemblyDebugging();
        }
        else {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();

        app.UseBlazorFrameworkFiles();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseSwagger();
        app.UseSwaggerUI(c => {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1");
        });

        app.MapRazorPages();
        app.MapControllers();
        app.MapFallbackToFile("index.html");
        
        app.Run();
    }
    
    private static void AddSwagger(WebApplicationBuilder builder) {
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen(options => {
            options.SwaggerDoc("v1", new OpenApiInfo {
                Title = "API", 
                Version = "v1",
                Description = "API for the Kanban Board application",
                TermsOfService = new Uri("https://example.com/terms"),
                Contact = new OpenApiContact {
                    Name = "Stefan Lieser",
                    Email = "info@ccd-akademie.de",
                    Url = new Uri("https://twitter.com/CCDAkademie"),
                },
                License = new OpenApiLicense {
                    Name = "Employee API LICX",
                    Url = new Uri("https://example.com/license"),
                }
            });     
            // Set the comments path for the Swagger JSON and UI.
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });
    }
}