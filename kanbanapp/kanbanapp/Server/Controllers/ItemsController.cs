using kanbanapp.contracts;
using kanbanapp.integration;
using Microsoft.AspNetCore.Mvc;

namespace kanbanapp.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ItemsController : ControllerBase
{
    private readonly Interactors _interactors;

    public ItemsController(Interactors interactors) {
        _interactors = interactors;
    }

    [HttpGet]
    public Board GetBoard() {
        return _interactors.Start();
    }

    [HttpPost]
    public Board PostNewItem(Item newItem) {
        return _interactors.CreateItem(newItem);
    }

    [HttpPost("left/{id}")]
    public Board MoveItemLeft(string id) {
        return _interactors.MoveLeft(id);
    }
    
    [HttpPost("right/{id}")]
    public Board MoveItemRight(string id) {
        return _interactors.MoveRight(id);
    }
    
    [HttpPost("up/{id}")]
    public Board MoveItemUp(string id) {
        return _interactors.MoveUp(id);
    }
    
    [HttpPost("down/{id}")]
    public Board MoveItemDown(string id) {
        return _interactors.MoveDown(id);
    }
}