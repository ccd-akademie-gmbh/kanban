namespace kanbanapp.contracts;

public class Item
{
    public Guid Id { get; set; } = Guid.NewGuid();
    
    public string Title { get; set; } = "";
}