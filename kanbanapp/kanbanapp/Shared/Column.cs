namespace kanbanapp.contracts;

public class Column
{
    public string Name { get; set; } = "";

    public int WIPLimit { get; set; }

    public List<Item> Items { get; set; } = new();
}