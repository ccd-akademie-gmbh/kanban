namespace kanbanapp.contracts;

public class Board
{
    public List<Column> Columns { get; set; } = new();
}