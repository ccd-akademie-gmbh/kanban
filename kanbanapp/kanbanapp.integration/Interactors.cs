﻿using kanbanapp.contracts;

namespace kanbanapp.integration;

public class Interactors
{
    private Board _board = new();

    public Board Start() {
        _board = new Board {
            Columns = new List<Column> {
                new() { Name = "ready", Items = {
                    new() { Title = "Eins" },
                    new() { Title = "Zwei" },
                    new() { Title = "Drei" }
                }},
                new() { Name = "doing", WIPLimit = 3, Items = {
                    new() { Title = "Vier" }
                }},
                new() { Name = "done", Items = {
                    new() { Title = "Fünf" }
                }}
            }
        };
        return _board;
    }

    public Board CreateItem(Item newItem) {
        _board.Columns[0].Items.Add(newItem);
        return _board;
    }

    public Board MoveLeft(string id) {
        var (item, column, position) = FindItem(id);
        if (item == null || column == 0) {
            return _board;
        }
        var fromColumn = column;
        var toColumn = column - 1;

        _board.Columns[fromColumn].Items.Remove(item);
        _board.Columns[toColumn].Items.Insert(Math.Min(position, _board.Columns[toColumn].Items.Count), item);

        return _board;
    }

    public Board MoveRight(string id) {
        var (item, column, position) = FindItem(id);
        if (item == null || column + 1 >= _board.Columns.Count) {
            return _board;
        }
        var fromColumn = column;
        var toColumn = column + 1;

        _board.Columns[fromColumn].Items.Remove(item);
        _board.Columns[toColumn].Items.Insert(Math.Min(position, _board.Columns[toColumn].Items.Count), item);

        return _board;
    }

    public Board MoveUp(string id) {
        var (item, column, position) = FindItem(id);
        if (item == null || position == 0) {
            return _board;
        }

        _board.Columns[column].Items.Remove(item);
        _board.Columns[column].Items.Insert(position - 1, item);

        return _board;
    }

    public Board MoveDown(string id) {
        var (item, column, position) = FindItem(id);
        if (item == null || position + 1 >= _board.Columns[column].Items.Count) {
            return _board;
        }

        _board.Columns[column].Items.Remove(item);
        _board.Columns[column].Items.Insert(position + 1, item);

        return _board;
    }

    private (Item? Item, int Column, int Position) FindItem(string id) {
        for (var column = 0; column < _board.Columns.Count; column++) {
            for (var position = 0; position < _board.Columns[column].Items.Count; position++) {
                var theItem = _board.Columns[column].Items[position];
                if (theItem.Id.ToString() == id) {
                    return (theItem, column, position);
                }
            }
        }
        return (null, 0, 0);
    }
}